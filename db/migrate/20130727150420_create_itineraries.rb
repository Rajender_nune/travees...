class CreateItineraries < ActiveRecord::Migration
  def change
    create_table :itineraries do |t|
      t.string :itinerary_title
      t.string :itinerary_slug
      t.text :itinerary_description
      t.string :itinerary_tag

      t.timestamps
    end
  end
end
