/********************************************************************************
 * jQuery.nextOrFirst()
 *
 * PURPOSE:  Works like next(), except gets the first item from siblings if there is no "next" sibling to get.
 * SOURCE: http://www.mattvanandel.com/999/jquery-nextorfirst-function-guarantees-a-selection/
 ********************************************************************************/
jQuery.fn.nextOrFirst = function(selector){
	var next = this.next(selector);
	return (next.length) ? next : this.prevAll(selector).last();
}

/********************************************************************************
 * jQuery.prevOrLast()
 * 
 * PURPOSE: Works like prev(), except gets the last item from siblings if there is no "prev" sibling to get.
 * SOURCE: http://www.mattvanandel.com/999/jquery-nextorfirst-function-guarantees-a-selection/
 ********************************************************************************/
jQuery.fn.prevOrLast = function(selector){
	var prev = this.prev(selector);
	return (prev.length) ? prev : this.nextAll(selector).last();
}





jQuery(document).ready(function(jQuery) { 
	/*
	 * Global 
	*/

	function init_js_styling( selector ){
		// Auto-Size
		if( selector.find('.autosize').length > 0 )
			selector.find('.autosize').autosize();

		// Tagging
		if( selector.find('.tagit').length > 0 )
			selector.find('.tagit').tagit();

		// Number only for input text
		if( selector.find('.field-budget').length > 0 )
			selector.find('.field-budget').autoNumeric();

		if( selector.find('.input-money').length > 0 )
			selector.find('.input-money').autoNumeric();

		// Currency custom dropdown
		if( selector.find('.select-currency').length > 0 )
			selector.find('.select-currency').fancyfields();	

		if( selector.find('.stream-sort').length > 0 )
			selector.find('.stream-sort').fancyfields({
				onSelectChange: function(){
					jQuery('#stream').html('<div class="loading-wrap"><img src="/assets/loading-autocomplete.gif" alt="Loading..." /></div>');
	
					// do something, than replace this .loading-wrap
				}
			});

		if( selector.find('.custom-select').length > 0)
			selector.find('.custom-select').fancyfields();

		if( selector.find('.scrolled-items').length > 0)
			selector.find('.scrolled-items').customScrollbar();
	}
	init_js_styling( jQuery('body') );

	function string_to_tags( text ){
		var text_array = text.split(',').reverse();
		var tags = '';

		for (var i = text_array.length - 1; i >= 0; i--) {
			if( i < (text_array.length - 1)) tags += ', ';
			tags += '<a href="#">'+text_array[i]+'</a>';
		};

		return tags;
	}
	string_to_tags('muka,loololo,barakalooo,opo');

	// Upload Image
	jQuery('#body').on( 'click', '.upload-image-wrap .trigger', function(e){
		e.preventDefault();
		var trigger_link = jQuery(this);
		trigger_link.parents('.upload-image-wrap').find('.field-upload').trigger('click');
	});

	jQuery('#body').on( 'click', '.upload-image-wrap .remove', function(e){
		e.preventDefault();
		var remove_link = jQuery(this);
		var upload_image_wrap = remove_link.parents('.upload-image-wrap');
		var field_upload = upload_image_wrap.find('.field-upload');

		remove_link.hide();
		upload_image_wrap.find('.trigger').show();
		upload_image_wrap.find('.preview-image').hide().find('img').remove();
		field_upload.replaceWith( field_upload = field_upload.clone( true ) );
	});

	jQuery('#body').on( 'change', '.upload-image-wrap .field-upload', function(e){
		var field_upload = jQuery(this);
		var image = e.target.files[0];
		var reader = new FileReader();
		
		reader.readAsDataURL( image );
		reader.onload = function(e){
			var image_url = e.target.result;
			field_upload.parents('.upload-image-wrap').find('.trigger').hide();
			field_upload.parents('.upload-image-wrap').find('.remove, .preview-image').show();	
			field_upload.parents('.upload-image-wrap').find('.preview-image').append('<img src="'+image_url+'" />');
		}
	});

	// Top Nav - Settings
	jQuery('#top-menu .has-sub-menu').click(function(e){
		e.preventDefault();

		var parent_menu = jQuery(this);
		parent_menu.toggleClass('active');
		parent_menu.find('.sub-menu').toggle();
	});

	jQuery('#top-menu-search').click(function(e){
		e.preventDefault();

		var menu_item = jQuery(this);
		menu_item.toggleClass('active');
		jQuery('#top-search').toggle();
		jQuery('#top-search .text').focus();
	});

	// Making sure there's no "hanging page"
	var window_height = jQuery(window).height();
	var body_wrapper_height = jQuery('#wrap-outer-all').outerHeight();
	if( body_wrapper_height < window_height ){
		jQuery('#footer-menu').height(window_height - body_wrapper_height + 20);
	}

	// Load More
	jQuery('#stream').on( 'click', '.load-more', function(e){
		e.preventDefault();
		var load_more = jQuery(this);
		load_more.html('<img src="images/loading-autocomplete.gif" title="Loading..." class="loading-image" />');

		// Do Something

		// Back to initial state
		// load_more.html('Load More');
	});

	// Lightbox
	function create_modal(){
		// Remove the previous one, if there's any
		jQuery('#modal').remove();

		// Append the new one
		jQuery('body').css({ 'overflow' : 'hidden' }).append('<div id="modal-wrap"><div id="modal"><img src="images/loading-autocomplete.gif" class="loading" /><a href="#" id="modal-close" title="Close">Close</a></div><div id="modal-background"></div></div>');
		jQuery('#modal-wrap').fadeIn();
	}

	function close_modal(){
		jQuery('#modal').css({ 'overflow' : 'hidden' }).animate({
			'top' : '-50%'
		}, 200);
		
		jQuery('#modal-wrap').fadeOut(function(){
			jQuery(this).remove();
			jQuery('body').css({ 'overflow' : 'visible' });			
		});
	}

	function insert_image_to_modal( image_src ){
		var img = jQuery('<img />').attr({
			'src' : image_src, 
			'style' : 'opacity:0;',
			'id'	: 'modal-image'
		}).load(function(){
			// Load the image
			jQuery('#modal').append(img);

			var window_width = jQuery(window).width();
			var window_height = jQuery(window).height();
			
			var max_modal_width = window_width * .7;
			var max_modal_height = window_height * .7;

			var img_width = img.width();
			var img_height = img.height();

			if ((img_width / img_height) > 1){
				// Landscape
				if (img_width > max_modal_width){
					// If the image is much larger than available area
					var img_to_modal_ratio = max_modal_width / img_width;
				} else {
					if ((max_modal_width * 0.9) < img_width){
						// If the image is quite near the available area, that'd be quite a problem especially if the ratio is near to 1
						var img_to_modal_ratio = .75;	
					} else {
						// If the image is much smaller than available area
						var img_to_modal_ratio = 1;
					}
				}
			} else {
				// Portrait
				if (img_height > max_modal_height){
					// If the image is much larger than available area
					var img_to_modal_ratio = max_modal_height / img_height;
				} else {
					if ((max_modal_height * 0.9) < img_height){
						// If the image is quite near the available area, that'd be quite a problem especially if the ratio is near to 1
						var img_to_modal_ratio = .75;
					} else {
						// If the image is much smaller than available area
						var img_to_modal_ratio = 1;						
					}
				}
			}
			
			var img_preview_width = img_width * img_to_modal_ratio;
			var img_preview_height = img_height * img_to_modal_ratio;
			
			jQuery('#modal .loading').remove();

			jQuery('#modal').animate({
				'width'	: img_preview_width,
				'height': img_preview_height,
				'margin-top' : 0 - (img_preview_height / 2),
				'margin-left' : 0 - (img_preview_width / 2),
				'border-radius' : 0,
				'padding' : 0,
				'border-width' : 0
			}, 400, function(){
					jQuery(this).find('img').css({
					'opacity' : 1,
					'width' : '100%'
				});
			});
		});		
	}

	// Summon Lightbox
	jQuery('.lightbox').click(function(e){
		e.preventDefault();
		var image_src = jQuery(this).attr('href');

		create_modal();
		insert_image_to_modal( image_src );
	});

	// Close Lightbox
	jQuery('body').on('click', '#modal-close, #modal-background', function(e){
		e.preventDefault();
		close_modal();
	});
	
	jQuery(document).keyup(function(e){
		if ( e.keyCode == 27 && jQuery('body #modal').length > 0){
			close_modal();
		}
	});
});