/* 
 * All javascript for itinerary add new page
 * Authored by Fikri Rasyid and Asep Bagja P.
 */

jQuery(document).ready(function() {

	// Toggle itenerary-todo's details	
	jQuery('#add-todos').on('keyup', '.todo-rows .itenerary-todo-field', function(){
		var input_val = jQuery(this).val();
		if( input_val != ''){
			jQuery(this).parents('.add-todo').find('.add-todo-details').slideDown();
			jQuery(this).parents('.add-todo').find('.add-todo-details-preview').slideUp();
		} else {
			jQuery(this).parents('.add-todo').find('.add-todo-details').slideUp();	
		}		
	});

	// Load the map into their container
	if( jQuery('#itenerary-map').length > 0 ){
		var itenerary_map = L.mapbox.map('itenerary-map', 'travelin7.map-a5ndp27o');

		// Map - Set Marker
		var itenerary_map_marker;
		itenerary_map.on( 'click', function(e){
			console.log(e);
			// Remove current marker if there's any
			if( itenerary_map.hasLayer(itenerary_map_marker) == true){
				itenerary_map.removeLayer(itenerary_map_marker);
			}

			// Add a marker
			itenerary_map_marker = new L.Marker(e.latlng);
			itenerary_map.addLayer(itenerary_map_marker);

			// Store the marker LatLng
			jQuery('.itenerary-latlon-field').val( e.latlng.lat + ',' + e.latlng.lng );
		});
	}
	// Map - Search
	jQuery('.map-search').keypress(function(e) {
		var search_input = jQuery(this);
		var code = (e.keyCode ? e.keyCode : e.which);

		if(code === 13) {
	    	var keyword = jQuery(this).val(); 
	    	var search_result_wrap = jQuery('.search-location-results');
			// Remove the result first
			search_result_wrap.find('ul').empty();
			
			// Get the data
	    	jQuery.ajax({
	    		url: 'http://0.0.0.0:3000/search_location/'+keyword,
	    		type: 'get',
	    		beforeSend: function(){
	    			search_input.addClass('ui-autocomplete-loading');
    				search_result_wrap.slideDown();
    				search_result_wrap.find('ul').prepend('<li>Fetching location data....</li>')
	    		},
	    		success: function(res){
	    			search_result_wrap.find('ul').empty();
	    			
	    			if(res.length > 0) {
	    				for (var i = res.length - 1; i >= 0; i--) {
	    					search_result_wrap.find('ul').prepend('<li><a href="#" data-latlon="'+res[i].data.lat+','+res[i].data.lon+'">'+res[i].data.display_name+'</a></li>');
		    			}
	    			} else {
	    				search_result_wrap.find('ul').prepend('<li>No location found. Try another keyword</li>');
	    			}
	    		},
	    		complete: function(){
	    			search_input.removeClass('ui-autocomplete-loading');	    			
	    		}
	    	});

	    	return false;
		}
	});

	jQuery('.search-location-results').on('click', 'ul li a', function(e){
		e.preventDefault();
		var loc = jQuery(this).attr('data-latlon');
		var loc_obj = loc.split(',');
		itenerary_map.setView( loc_obj, 10 );

		// Add a marker
		itenerary_map_marker = new L.Marker(loc_obj);
		itenerary_map.addLayer(itenerary_map_marker);

		// Store the marker LatLng
		jQuery('.itenerary-latlon-field').val( loc );		

		// Close the search result wrapper
		jQuery('.search-location-results').slideUp();
	});  


	// Importing data from Garmin Device
	// When going live, chance the key pair site. Don't forget
	var garminGpsPluginReference = null;

	if(findPlugin() === true) {
		var pluginUnlocked = unlockPluginAndStartFindingDevices(["http://0.0.0.0:3000/itinerary/new","41fb6b670aa6705b5c7e5c63e631f16b"]);

		if(pluginUnlocked !== null) {
			console.log("plugin unlocked");
		} else {
			console.log("Plugin could not be unlocked.");
		}	
	} else {
		console.log("Plugin NOT Found.");
	}

	garminGpsPluginReference = pluginUnlocked;

	jQuery('#use-garmin-device').bind('click', {reference: garminGpsPluginReference}, function(e) {
		e.preventDefault();
		
		var reference = e.data.reference;

		var finishedLooking = reference.finishFindingDevices();
		var deviceDescription = reference.getDescriptionOfDevices();
		
		if(finishedLooking && deviceDescription !== null) {
			var xmlDoc     = jQuery.parseXML(deviceDescription);
			var deviceNode = jQuery(xmlDoc).find('Device')[0];
			var deviceName = jQuery(deviceNode).attr('DisplayName');

			// show the status
			jQuery('.loading').hide();
			jQuery('#device-name').text(deviceName);
			jQuery('.importing').show();

			// read the data
			startReadGpsFromFirstDevice();

			jQuery('#garmin-stop-importing').click(function(e) {
				e.preventDefault();
				var readData =  finishReadAndGetData();
				console.log(readData);
			});
		} else {
			console.log("No Devices found.");
		}

		jQuery('#garmin-importer-wrap').slideDown();
	});

	jQuery('#garmin-importer-close').click(function(e) {
		e.preventDefault();
		jQuery('#garmin-importer-wrap').slideUp();
	});

	// Determine the current location of the user
	// For the traveller at their trip
	jQuery('#use-current-location').click(function(e) {
		e.preventDefault();
		if(!navigator.geolocation) {
			console.log("Geolocation is not support in your browser.");
		}

		function success(position) {
			var latitude  = position.coords.latitude;
			var longitude = position.coords.longitude;

			console.log(latitude+" "+longitude);
		}

		function error() {
			console.log("Unable to retrieve your location.");
		}

		navigator.geolocation.getCurrentPosition(success(), error());
	});

	// Todo - Location Settings
	jQuery('.todo-rows').on( 'change', '.field-todo-location-setting', function(){
		var location_setting = jQuery(this);
		var location_setting_val = location_setting.val();
		var add_todo = location_setting.parents('.add-todo');
		var todo_latlon = add_todo.find('.field-todo-location');
		var itenerary_latlon_val = jQuery('.itenerary-latlon-field').val();
		var itenerary_latlon_obj = itenerary_latlon_val.split(',');
		var todo_map = add_todo.find('.todo-map');
		var todo_map_id = todo_map.attr('id');		

		// By default, todo's location is equal to itenerary's location
		todo_latlon.val( itenerary_latlon_val );

		// However, if user wish to use more specific location:
		if( location_setting_val == 'custom'){
			// var todo_map = L.mapbox.map(todo_map_id, 'fikrirasyid.map-zli88u36').setView( todo_latlon_obj, 10 );	
			add_todo.find('.location-rows').slideDown( 400, function(){
				init_todo_mapbox( todo_map_id, itenerary_latlon_obj );				
			});
		} else{
			add_todo.find('.location-rows').slideUp();
		}

		console.log( itenerary_latlon_val );
	});

	function init_todo_mapbox( todo_map_id, todo_latlon_obj ){
		var todo_map = L.mapbox.map(todo_map_id, 'fikrirasyid.map-zli88u36').setView( todo_latlon_obj, 10 );	

		var todo_map_marker;
		todo_map.on( 'click', function(e){
			// Remove current marker if there's any
			if( todo_map.hasLayer(todo_map_marker) == true){
				todo_map.removeLayer(todo_map_marker);
			}

			// Add a marker
			todo_map_marker = new L.Marker(e.latlng);
			todo_map.addLayer(todo_map_marker);

			// Store the marker LatLng
			jQuery('#'+todo_map_id).parents('.todo-map-wrap').find('.field-todo-location').val( e.latlng.lat + ',' + e.latlng.lng );
		});	
	}		
	
	// Todo - Button Add
	jQuery('#page-add-new .todo-rows').on('click', '.add-todo .add-todo-button', function(e){
		e.preventDefault();

		// Caching selectors
		var add_todo_button = jQuery(this);
		var add_todo = add_todo_button.parents('.add-todo');
		var add_todo_details = add_todo.find('.add-todo-details');
		var add_todo_details_preview = add_todo.find('.add-todo-details-preview');

		// Perform To Do Saving Action Here - 4bepitulaz

		// Print Preview
		var description = add_todo.find('.field-description').val();
		var tags_string = add_todo.find('.field-tags').val();
		var tags = tags_string.split(",");
		var currency = add_todo.find('.field-currency').val();
		var budget = add_todo.find('.budget').val();
		var location = add_todo.find('.field-todo-location').val();

		add_todo_details_preview.find('.preview-description').text(description);
		add_todo_details_preview.find('.preview-tags').empty();
		for( var key in tags){
			if( key > 0 ){
				add_todo_details_preview.find('.preview-tags').append(', ');
			}
			add_todo_details_preview.find('.preview-tags').append('<a href="">'+tags[key]+'</a>')
		}
		add_todo_details_preview.find('.preview-currency').text(currency);
		add_todo_details_preview.find('.preview-budget').text(budget);
		add_todo_details_preview.find('.preview-location').html('<a href="">'+location+'</a>')

		// Where's my preview?
		add_todo_details.slideUp();
		add_todo_details_preview.slideDown();

		// Update budget 
		update_todos_budget();
	});

	// Todo - Button Cancel
	jQuery('.todo-rows').on('click', '.add-todo .add-todo-cancel', function(e){
		e.preventDefault();		
	});	

	// Todo - Edit From Preview
	jQuery('.todo-rows').on('click', '.preview-edit', function(e){
		e.preventDefault();

		var edit = jQuery(this);
		var add_todo = edit.parents('.add-todo');
		var add_todo_details = add_todo.find('.add-todo-details');
		var add_todo_details_preview = add_todo.find('.add-todo-details-preview');

		// Close Preview, Open Details		
		add_todo_details_preview.slideUp();		
		add_todo_details.slideDown();
	});

	// Todo - Delete From Preview
	jQuery('.todo-rows').on('click', '.preview-delete', function(e){
		e.preventDefault();

		var delete_button = jQuery(this);
		var add_todo = delete_button.parents('.add-todo');

		var confirm_deleting = window.confirm("You are about to delete this todo. Are you sure?");

		if(confirm_deleting == true){
			add_todo.fadeOut(400, function(){
				add_todo.remove();

				// Update budget 
				update_todos_budget();
			});
		}		
	});

	// Todo - Add More Todo
	jQuery('.todo-rows').on('click', '.add-more a', function(e){
		e.preventDefault();
		var current_count_todo = jQuery('#add-todos .add-todo').size();
		var add_todo_id = 'add-todo-' + Math.round(new Date().getTime() / 1000);
		var template = jQuery('#template-add-todo').html();

		jQuery('#add-todos').append(template);
		jQuery('#add-todos .add-todo:last').attr('id', add_todo_id);

		jQuery('#'+add_todo_id).find('.autosize').autosize();
		jQuery('#'+add_todo_id).find('.tagit').tagit();
		jQuery('#'+add_todo_id).find('.field-budget').autoNumeric();
		jQuery('#'+add_todo_id).find('.input-money').autoNumeric();
		jQuery('#'+add_todo_id).find('.select-currency').fancyfields();		
	});

	// Budget
	function update_todos_budget(){

		// Create an array of all todos' budget
		var todos_budgets = new Array();
		jQuery('.add-todo .budget').each(function(){
			todos_budgets.push(jQuery(this).autoNumeric('get'));
		});

		// Loop and sum all of todos' budget
		var todos_budgets_summed = 0;
		for (var i = todos_budgets.length - 1; i >= 0; i--) {
			budget = parseInt(todos_budgets[i]);
			if(typeof budget != NaN) todos_budgets_summed += budget;
		};

		if(isNaN(todos_budgets_summed)){
			todos_budgets_summed = 0;
		}

		// Display the result
		jQuery('#todo-budget').autoNumeric('set', todos_budgets_summed);

		// Get "More Budget" value
		var more_budget = jQuery('#more-budget').autoNumeric('get');

		// Calculate and push total budget to its place
		jQuery('#total-budget').autoNumeric('set', parseInt(todos_budgets_summed) + parseInt(more_budget));
		console.log( todos_budgets_summed );
		console.log(more_budget);
	}

	jQuery('#more-budget').keypress(function(){
		update_todos_budget();
	});
});