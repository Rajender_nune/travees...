module ApplicationHelper
	# saving the user into session
	def sign_in(user_id, username)
		session[:user_id]  = user_id
		session[:username] = username
	end

	#check if the user has been signed in
	def user_signed_in
		@user_id  = session[:user_id]
		@username = session[:username]
	end
end
