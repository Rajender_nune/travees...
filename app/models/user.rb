class User < ActiveRecord::Base

	has_secure_password

	validates :username, presence: true, length: { minimum: 3, maximum: 20 }, uniqueness: true
	validates :fullname, presence: true
	validates :email, presence: true, uniqueness: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/ }
	validates :password, confirmation: true, length: { minimum: 6 }
	validates :password_confirmation, presence: true
	validates :agreement, acceptance: { accept: 'yes' }

end
