class ItineraryController < ApplicationController
  def index
  end

  def new
    user_signed_in
  end

  def search
    @places = Geocoder.search(params[:query_string])
    render json: @places
  end

  def show
  end

  def create
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
