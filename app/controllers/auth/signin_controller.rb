class Auth::SigninController < ApplicationController

  def index

  end

  # Process the login form
  def create
  	user = User.find_by_username(params[:users][:username])

    if user && user.authenticate(params[:users][:password])
      sign_in user.id, user.username
      redirect_to home_path
    else
      redirect_to auth_sign_in_path, alert: I18n.t('login_fail')
    end

  end

  # destroy the session
  def destroy
  	reset_session

  	redirect_to root_path
  end

end
