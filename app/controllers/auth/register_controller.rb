class Auth::RegisterController < ApplicationController
  def index
    @user = User.new
  end

  # Create the new user
  # to do: 
  #    - after successful, add the parameter into session
  #    - if fail, don't redirect. but use render so the information in field never lost
  def create
  	@user = User.new user_params

  	if @user.save
  		redirect_to home_path, notice: I18n.t('register_successful')
  	else
      redirect_to auth_sign_up_path, notice: I18n.t('register_fail')
  	end
  end

  private
  def user_params
    params.require(:users).permit(:username, :fullname, :email, :password, :password_confirmation, :agreement)
  end
end
